import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import AUIBreadcrumbs from '../src/AUIBreadcrumbs';

describe('AUIBreadcrumbs', () => {
    it('should render the correct AUI markup', () => {
        expect(render(<AUIBreadcrumbs />).html()).to.equal('<ol class="aui-nav aui-nav-breadcrumbs"></ol>');
    });
});
