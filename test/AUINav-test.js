import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import AUINav from '../src/AUINav';

describe('AUINav', () => {
    it('should render an unordered list with the "aui-nav" class', () => {
        expect(render(<AUINav />).html()).to.equal(`<ul class="aui-nav"></ul>`);
    });

    it('should render a single li child inside the container', () => {
        expect(render(<AUINav><li>test</li></AUINav>).html()).to.equal(`<ul class="aui-nav"><li>test</li></ul>`);
    });

    it('should render multiple li children inside the container', () => {
        expect(render(<AUINav><li>test</li><li><a href="#">test2</a></li></AUINav>).html()).to.equal(`<ul class="aui-nav"><li>test</li><li><a href="#">test2</a></li></ul>`);
    });

    it('should render data options inside the container', () => {
        const mockOptions = [
            <a>Link 1</a>,
            <a to="someUrl">Link 2</a>
        ];

        expect(render(<AUINav options={mockOptions} />).html()).to.equal(`<ul class="aui-nav"><li><a>Link 1</a></li><li><a to="someUrl">Link 2</a></li></ul>`);
    });
});
