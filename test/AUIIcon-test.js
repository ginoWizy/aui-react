import React from 'react';
import { shallow } from 'enzyme';

import { expect } from 'chai';
import AUIIcon from '../src/AUIIcon';

describe('AUIIcon', () => {
    it('should render the correct AUI markup', () => {
        const wrapper = shallow(<AUIIcon icon="configure"/>);
        expect(wrapper.html())
            .to.equal(`<span class="aui-icon aui-icon-small aui-iconfont-configure"></span>`);
    });

    it('should render the correct AUI markup from non-iconfont icons', () => {
        const wrapper = shallow(<AUIIcon icon="wait" useIconFont={false}/>);
        expect(wrapper.html())
            .to.equal(`<span class="aui-icon aui-icon-small aui-icon-wait"></span>`);
    });

    it('should render children inside the AUI markup', () => {
        const wrapper = shallow(<AUIIcon icon="disabled">Disabled</AUIIcon>);
        expect(wrapper.html())
            .to.equal(`<span class="aui-icon aui-icon-small aui-iconfont-disabled">Disabled</span>`);
    });

    it('should render a background style for custom icons', () => {
        const wrapper = shallow(<AUIIcon customIcon="/myawesomeicon.svg"/>);
        expect(wrapper.html())
            .to.equal(`<span class="aui-icon aui-icon-small" style="background-image:url(/myawesomeicon.svg);"></span>`);
    });

    it('should render the correct size class for medium icons', () => {
        const wrapper = shallow(<AUIIcon icon="configure" size="medium"/>);
        expect(wrapper.html())
            .to.equal(`<span class="aui-icon aui-icon-medium aui-iconfont-configure"></span>`);
    });

    it('should render the correct size class for large icons', () => {
        const wrapper = shallow(<AUIIcon icon="configure" size="large"/>);
        expect(wrapper.html())
            .to.equal(`<span class="aui-icon aui-icon-large aui-iconfont-configure"></span>`);
    });

    it('should render the correct markup for specified className', () => {
        const wrapper = shallow(<AUIIcon icon="like" className="icon-like" />);
        expect(wrapper.html())
            .to.equal(`<span class="aui-icon aui-icon-small aui-iconfont-like icon-like"></span>`);
    });
});
