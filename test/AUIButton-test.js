import React from 'react';
import { render, shallow } from 'enzyme';

import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
chai.use(chaiEnzyme());


import AUIIcon from '../src/AUIIcon';
import AUIButton from '../src/AUIButton';

describe('AUIButton', () => {
    it('should render the correct AUI markup for a default button', () => {
        expect(render(<AUIButton>Default button</AUIButton>).html()).to.equal(`<button class="aui-button">Default button</button>`);
    });

    it('should render the correct AUI markup for a primary button', () => {
        expect(render(<AUIButton type="primary">Primary</AUIButton>).html()).to.equal(`<button class="aui-button aui-button-primary">Primary</button>`);
    });

    it('should render the correct AUI markup for a subtle button', () => {
        expect(render(<AUIButton type="subtle">Subtle</AUIButton>).html()).to.equal(`<button class="aui-button aui-button-subtle">Subtle</button>`);
    });

    it('should render an AUI icon inside the button when an icon is provided', () => {
        const wrapper = shallow(<AUIButton icon="configure">Icon button</AUIButton>);

        expect(wrapper).to.have.descendants('AUIIcon');
        expect(wrapper.find('AUIIcon')).to.have.descendants('[icon="configure"]');
    });
});
