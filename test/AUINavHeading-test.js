import React from 'react';
import { render } from 'enzyme';

import { expect } from 'chai';
import AUINavHeading from '../src/AUINavHeading';

describe('AUINavHeading', () => {
    it('should render the correct AUI markup', () => {
        expect(render(<AUINavHeading>My Heading</AUINavHeading>).html()).to.equal(`<div class="aui-nav-heading"><strong>My Heading</strong></div>`);
    });
});
