import React from 'react';
import { render } from 'enzyme';

import { expect } from 'chai';
import AUITable from '../src/AUITable';

describe('AUITable', () => {
    it('should render the correct AUI markup', () => {
        expect(
            render(<AUITable>
                <tbody>
                    <tr><td>test</td></tr>
                </tbody>
            </AUITable>).html()
        ).to.equal('<table class="aui"><tbody><tr><td>test</td></tr></tbody></table>');
    });
});
