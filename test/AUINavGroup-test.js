import React from 'react';
import { render } from 'enzyme';
import { expect } from 'chai';
import AUINavGroup from '../src/AUINavGroup';

describe('AUINavGroup', () => {
    it('should render the correct AUI markup', () => {
        expect(render(<AUINavGroup />).html()).to.equal(`<nav class="aui-navgroup aui-navgroup-vertical"><div class="aui-navgroup-inner"></div></nav>`);
    });
});
