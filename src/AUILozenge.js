import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

export default class AUILozenge extends Component {
    render() {
        const classes = classnames({
            'aui-lozenge': true,
            'aui-lozenge-subtle': this.props.subtle
        }, 'aui-lozenge-' + this.props.type);
        return (
            <span className={classes}>{this.props.children}</span>
        );
    }
}

AUILozenge.displayName = 'AUILozenge';

AUILozenge.propTypes = {
    type: PropTypes.oneOf(['success', 'error', 'current', 'new', 'moved']),
    subtle: PropTypes.bool,
    children: PropTypes.string
};
