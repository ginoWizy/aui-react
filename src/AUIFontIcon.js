import React from 'react';
import classnames from 'classnames';

const AUIFontIcon = props => {
    const classes = classnames('aui-icon', {
        'aui-icon-small': props.size === 'small'
    }, 'aui-iconfont-' + props.type);

    return (<span className={classes} {...props} />);
};

AUIFontIcon.displayName = 'AUIFontIcon';
AUIFontIcon.propTypes = {
    size: React.PropTypes.oneOf(['small']),
    type: React.PropTypes.string,
    title: React.PropTypes.string
};

export default AUIFontIcon;
