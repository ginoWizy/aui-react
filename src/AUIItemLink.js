import React, { Component, PropTypes } from 'react';

const AUIItemLink = ({ className, href, anchorClass, children, ...otherProps }) => (
    <li className={className}>
        <a href={href} className={anchorClass} {...otherProps}>
            {children}
        </a>
    </li>
);

export default AUIItemLink;
