import React, { PropTypes } from 'react';
import AUINav from './AUINav';
import AUINavHeading from './AUINavHeading';
import classnames from 'classnames';

const AUISidebarGroup = props => {
    const { groupId, tier, heading, options, children, className } = props;
    const classes = classnames('aui-sidebar-group', `aui-sidebar-group-tier-${tier}`, groupId, className);

    const header = (typeof heading === 'string') ? <AUINavHeading>{heading}</AUINavHeading> : heading;

    return (
        <div id={groupId} style={{paddingTop:15}} className={classes}>
            <div className="aui-navgroup-inner">
                { header }

                <AUINav options={options}>{children}</AUINav>
            </div>
        </div>
    );
};

AUISidebarGroup.displayName = 'AUISidebarGroup';

AUISidebarGroup.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node,
    options: PropTypes.array,
    groupId: PropTypes.string.isRequired,
    heading: React.PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]).isRequired,
    tier: PropTypes.string
};

AUISidebarGroup.defaultProps = {
    tier: 'one'
};

export default AUISidebarGroup;
