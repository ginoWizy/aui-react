import React from 'react';

const AUINav = ({options, children}) => {
    let dataDrivenOptions;

    if (options) {
        dataDrivenOptions = options.map(option => {
            return (
                <li key={option.props.to}>
                    {option}
                </li>
            );
        });
    }

    return (
        <ul className="aui-nav">
            {dataDrivenOptions}
            {children}
        </ul>
    );
};

AUINav.propTypes = {
    onChange: React.PropTypes.func,
    options: React.PropTypes.array,
    selected: React.PropTypes.string,
    children: React.PropTypes.node
};

AUINav.displayName = 'AUINav';

export default AUINav;
