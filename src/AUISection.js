import React from 'react';

const AUISection = ({ children, ...otherProps }) => (
    <aui-section {...otherProps}>{children}</aui-section>
);

AUISection.displayName = 'AUISection';
AUISection.propTypes = {
    children: React.PropTypes.element
};

export default AUISection;
