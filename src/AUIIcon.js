import React, { PropTypes } from 'react';
import classnames from 'classnames';

const AUIIcon = props => {
        let iconfontClass;
        let customStyle;
        let className = props.className;
        const { icon, size, style, useIconFont, customIcon, onClick, onMouseEnter, onMouseLeave, children } = props;

        if (icon) {
            iconfontClass = useIconFont ? `aui-iconfont-${icon}` : `aui-icon-${icon}`;
        }

        className = classnames(
            'aui-icon',
            `aui-icon-${size}`,
            iconfontClass,
            className
        );

        customStyle = style || {};
        if (customIcon) {
            customStyle.backgroundImage = `url(${customIcon})`;
        }

        return <span className={className} style={customStyle} onClick={onClick} onMouseEnter={onMouseEnter} onMouseLeave={onMouseLeave}>{children}</span>;
};

AUIIcon.defaultProps = {
    useIconFont: true,
    size: 'small'
};
AUIIcon.displayName = 'AUIIcon';
AUIIcon.propTypes = {
    /**
     * Icon innerHtml text/code (could be a string or an element)
     */
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string
    ]),

    /**
     * Icon specific style
     */
    style: PropTypes.object,

    /**
     * Type of icon size
     */
    size: PropTypes.oneOf(['small', 'medium', 'large']),

    /**
     * Icon kind
     */
    icon: PropTypes.string,

    /**
     * Replacement for Icon `background-image`
     */
    customIcon: PropTypes.string,

    /**
     * Custom ClassName
     */
    className: PropTypes.string,

    /**
     * Flag to define we are using icon of iconfont
     */
    useIconFont: PropTypes.bool,

    /**
     * A click event bound on the icon
     */
    onClick: PropTypes.func,

    /**
     * A mouse entering event bound on the icon
     */
    onMouseEnter: PropTypes.func,

    /**
     * A mouse leaving event bound on the icon
     */
    onMouseLeave: PropTypes.func
};

export default AUIIcon;
