import React, { Component, PropTypes } from 'react';

const AUIBlanket = props => {
    return (
        <div className='aui-blanket' tabIndex='0' style={{zIndex: 2980, ...props.style}} aria-hidden={false} onClick={props.onClick}></div>
    );
};

AUIBlanket.displayName = 'AUIBlanket';
AUIBlanket.propTypes = {
    /**
     * Custom style for the mask
     */
    style: PropTypes.object,
    /**
     * Click event to trigger on an element
     */
    onClick: PropTypes.func
};

export default AUIBlanket;
