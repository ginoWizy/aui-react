import React, { PropTypes } from 'react';
import classnames from 'classnames';

const AUINavItem = props => {
    const classes = classnames({'aui-nav-selected': props.selected});
    return (
        <li className={classes}>
            <a href={props.href} className="aui-nav-item">{props.children}</a>
        </li>
    );
};

AUINavItem.displayName = 'AUINavItem';
AUINavItem.propTypes = {
    href: PropTypes.string,
    selected: PropTypes.bool,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.string
    ])
};

export default AUINavItem;
