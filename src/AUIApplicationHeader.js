import React, { Component, PropTypes } from 'react';
import AUIApplicationLogo from './AUIApplicationLogo';

export default class AUIApplicationHeader extends Component {
    render() {
        return (
            <header id="header" role="banner">
                <nav className="aui-header aui-dropdown2-trigger-group" data-aui-response="true" role="navigation">
                    <div className="aui-header-inner">
                        <div className="aui-header-primary">
                            <AUIApplicationLogo logo={this.props.logo} text={this.props.text} headerLink={this.props.headerLink ? this.props.headerLink : '/'} />
                            {this.props.primaryContent}
                        </div>
                        <div className="aui-header-secondary">
                            {this.props.secondaryContent}
                        </div>
                    </div>
                </nav>
            </header>
        );
    }
}

AUIApplicationHeader.displayName = 'AUIApplicationHeader';
AUIApplicationHeader.propTypes = {
    logo: PropTypes.oneOf(['aui', 'bamboo', 'bitbucket', 'confluence', 'crowd', 'fecru', 'hipchat', 'jira', 'stash']).isRequired,
    headerLink: PropTypes.string,
    primaryContent: PropTypes.element,
    secondaryContent: PropTypes.string,
    text: PropTypes.string
};
