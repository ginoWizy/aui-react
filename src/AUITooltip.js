import React, { PropTypes } from 'react';

const AUITooltip = props => {
    const { gravity, children, ...otherProps } = props;
    return (
        <div role='tooltip' className={`tipsy tipsy-${gravity}`} {...otherProps}>
            <div className={`tipsy-arrow tipsy-arrow-${gravity}`}/>
            <div className='tipsy-inner'>{children}</div>
        </div>
    );
};

AUITooltip.displayName = 'AUITooltip';

AUITooltip.defaultProps = {
    style: {}
};

AUITooltip.propTypes = {
    /**
     * Children elements such as title for the tooltip
     */
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.element
    ]),
    /**
     * Direction and position for the tooltip
     */
    gravity: PropTypes.oneOf(['n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw']).isRequired,
    /**
     * Custom style
     */
    style: PropTypes.object,
    /**
     * On mouse enter event handler
     */
    onMouseEnter: PropTypes.func,
    /**
     * On mouse leave event handler
     */
    onMouseLeave: PropTypes.func
};

export default AUITooltip;
