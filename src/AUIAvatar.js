import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';

const AUIAvatar = props => {
    const classes = classnames('aui-avatar', {
        'aui-avatar-project': props.isProject === true
    }, 'aui-avatar-' + props.size);
    return (
        <span className={classes}>
            <span className="aui-avatar-inner">
                <img src={props.src}/>
            </span>
        </span>
    );
};

AUIAvatar.displayName = 'AUIAvatar';
AUIAvatar.propTypes = {
    src: PropTypes.string,
    size: PropTypes.oneOf(['xsmall', 'small', 'medium', 'large', 'xlarge', 'xxlarge', 'xxxlarge']),
    isProject: PropTypes.bool
};

export default AUIAvatar;
