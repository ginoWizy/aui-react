import React, { Component, PropTypes } from 'react';

export default class AUIBanner extends Component {
    render() {
        return (<div className="aui-banner aui-banner-error" role="banner" aria-hidden="false">
            {this.props.children}
        </div>);
    }
}

AUIBanner.displayName = 'AUIBanner';

AUIBanner.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.array
    ])
};
