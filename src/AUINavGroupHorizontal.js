import React, { PropTypes } from 'react';

const AUINavGroupHorizontal = props => {
        return (
            <nav className="aui-navgroup aui-navgroup-horizontal">
                <div className="aui-navgroup-inner">
                    <div className="aui-navgroup-primary">
                        {props.primary}
                    </div>
                    <div className="aui-navgroup-secondary">
                        {props.secondary}
                    </div>
                </div>
            </nav>
        );
};

AUINavGroupHorizontal.displayName = 'AUINavGroupHorizontal';
AUINavGroupHorizontal.propTypes = {
    primary: PropTypes.element,
    secondary: PropTypes.element
};

export default AUINavGroupHorizontal;
