import React, { PropTypes } from 'react';

const AUIPageHeaderImage = props => {
    return (
        <div className="aui-page-header-image">
            <span className="aui-avatar aui-avatar-large aui-avatar-project">
                <span className="aui-avatar-inner">{props.children}</span>
            </span>
        </div>
    );
};

AUIPageHeaderImage.displayName = 'AUIPageHeaderImage';
AUIPageHeaderImage.propTypes = {
    children: PropTypes.element
};

export default AUIPageHeaderImage;
