import React from 'react';

const AUINavHeading = props => {
    return <div className="aui-nav-heading"><strong>{props.children}</strong></div>;
};

AUINavHeading.displayName = 'AUINavHeading';
AUINavHeading.propTypes = {
    children: React.PropTypes.any
};

export default AUINavHeading;
