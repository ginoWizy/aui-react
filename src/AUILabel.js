import React, { Component, PropTypes } from 'react';

const AUILabel = props => {
    return <a className="aui-label">{props.children}</a>;
};

AUILabel.displayName = 'AUILabel';
AUILabel.propTypes = {
    children: PropTypes.string
};

export default AUILabel;
